use std::env;
use std::fs::{self, File};
use std::io::prelude::*;
use std::path::Path;
use std::cmp::Ordering;
use std::cmp::Ordering::*;
use std::iter::repeat;

struct FileCounts {
	name: String,
	lines: u64,
	words: u64,
	chars: u64,
}

impl FileCounts {
	fn new(name: String, lines: u64, words: u64, chars: u64) -> FileCounts {
		FileCounts {
			name,
			lines,
			words,
			chars,
		}
	}

	fn cmp(&self, other: &FileCounts, val: &str) -> Ordering {
		let mut a: u64 = 0;
		let mut b: u64 = 0;

		if val == "lines" {
			a = self.lines;
			b = other.lines;
		} else if val == "words" {
			a = self.words;
			b = other.words;
		} else if val == "chars" {
			a = self.chars;
			b = other.chars;
		} else if val == "name" {
			a = self.name.len() as u64;
			b = other.name.len() as u64;
		}
		if a == b {
			return Equal;
		} else if a > b {
			return Less;
		} else {
			return Greater;
		}
	}
}

fn main() {
    let args: Vec<_> = env::args().collect();
    let mut files: Vec<String> = Vec::new();
    let first_file: &Path;
    let mut file_structs: Vec<FileCounts>;
    let mut longest_filename: i64 = 0;

    if args.len() == 1 {
    	print_help();
    	return;
    }
    first_file = Path::new(&args[1]); 
    if args.len() == 2 && first_file.is_dir() {
    	files = visit_dirs(&Path::new(&args[1]), files).expect("Failed to get files");
    } else {
    	files = args[1..].to_vec();
    }
    file_structs = read_files(&files);
    for i in 0 .. file_structs.len() {
    	if file_structs[i].name.len() as i64 > longest_filename {
    		longest_filename = file_structs[i].name.len() as i64;
    	}
    }
    file_structs.sort_by(|a, b| a.cmp(b, "chars"));
    println!("Printing values");
    print_values(&file_structs, longest_filename + 2);
}

fn visit_dirs(dir: &Path, mut files: Vec<String>)-> Result<Vec<String>, String> {
	if dir.is_dir() {
		let paths = fs::read_dir(dir).unwrap();
		for path in paths {
			let secpath = path.unwrap().path();
			if secpath.is_dir() {
				files = visit_dirs(&secpath, files)?;
			} else  if secpath.is_file() && !secpath.symlink_metadata().unwrap().file_type().is_symlink() {
				files.push(String::from(secpath.to_str()
					.expect("Failed to convert filename to string")));
			}
		}
	}
	Ok(files)
}

fn read_files(filenames: &[String]) -> Vec<FileCounts> {
	let mut filetexts: Vec<FileCounts> = Vec::new();

	for filename in filenames {
		let tmp = File::open(filename); //.expect(&format!("{}{}", "Unable to open file ", filename));
		if tmp.is_ok() {
			let mut file = tmp.unwrap();
			let mut text: Vec<u8> = Vec::new();
			file.read_to_end(&mut text)
				.expect(&format!("{}{}", "Unable to read file ", filename));
			let mut filecount = FileCounts::new(filename.to_string(), 0, 0, 0);
			count_file(&mut filecount, &mut text);
			filetexts.push(filecount);
		}
	}
	filetexts
}

fn count_file(file: &mut FileCounts, text:&mut Vec<u8>) {
	let mut word: bool = true;
	let mut diff: u64 = 0;

	for i in 0..text.len() {
		if text[i] == 10 {
			file.lines += 1;
		}

		// Count words by finding whitespace or the begining or the end of a string.
		if is_whitespace (text[i]) || i == text.len() - 1 {
			if word && diff > 1 {
				file.words += 1;
				word = false;
				diff = 0;
			} else if diff > 1 {
				word = true;
				file.words += 1;
				diff = 0;
			}
		}
		file.chars += 1;
		diff += 1;
	}
}

fn count_total(file_structs: &[FileCounts]) -> (u64, u64, u64) {
	let mut lines: u64 = 0;
	let mut words: u64 = 0;
	let mut chars: u64 = 0;

	for file in file_structs {
		lines += file.lines;
		words += file.words;
		chars += file.chars;
	}
	(lines, words, chars)
}

fn print_values(file_structs: &[FileCounts], largest_filename: i64) {
	let largest_num: i64 = file_structs[0].words.to_string().len() as i64;
	let title_offset = largest_filename / 2 - 5;

	// Print out the help
	println!("{} {} {} {} {} {} {} {}",	
			repeat_char(' ', title_offset), "FILENAMES",
			repeat_char(' ', title_offset - 1), "LINES",
			repeat_char(' ', largest_num), "WORDS",
			repeat_char(' ', largest_num), "CHARACTERS");
	
	// Print out top border
	println!("{}", 	repeat_char('*', (largest_filename + 7) + (largest_num + 5) * 3));
	
	// Print out the files and their info
	for i in 0 .. file_structs.len() {
    	let file = &file_structs[i];
    	print!("{} {}", file.name, 			repeat_char(' ', largest_filename - file.name.len() as i64));
    	print!("{} {} {} {} {}", file.lines,repeat_char(' ', (largest_num + 5) - file.lines.to_string().len() as i64), file.words,
    										repeat_char(' ', (largest_num + 5) - file.words.to_string().len() as i64), file.chars);
    	println!("{}", repeat_char(' ', 8 - file.chars.to_string().len() as i64));
    }

   	// Get totals
    let (line_total, word_total, char_total) = count_total(&file_structs);
    
    // Print bottom border
    println!("{}", repeat_char('*', (largest_filename + 7) + (largest_num + 5) * 3));
    
    // Print out sums
    println!("SUM: {} {} {} {} {} {}", 	repeat_char(' ', largest_filename - 5), line_total,
    									repeat_char(' ', (largest_num + 5) - line_total.to_string().len() as i64), word_total,
    									repeat_char(' ', (largest_num + 5) - word_total.to_string().len() as i64), char_total);
    
    // Print bottom border
    println!("{}", repeat_char('*', (largest_filename + 7) + (largest_num + 5) * 3));
    println!("Largest: {}", largest_filename);
}

fn repeat_char(c: char, len: i64) -> String {
	let newlen: usize;
	if len < 0 {
		newlen = 0;
	} else {
	 	newlen = len as usize;
	}
	repeat(c).take(newlen).collect::<String>()
}

fn is_whitespace(c: u8) -> bool {
	c == 0x20 || c == 0x0c || c == 0x0a ||
	c == 0x0d || c == 0x09 || c == 0x0b 
}

fn print_help() {
	println!("Help:\n\t./count filename ...\n\t./count directory");
}
